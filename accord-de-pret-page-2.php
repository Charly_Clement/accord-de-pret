<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="accord-de-pret-page-2.css">
    <title>Accord de prêt - Page 2</title>
</head>

<body>

    <div class="entete-page2">
        <h2>Accord de prêt</h2>
        <a class="déconnection" href="?sup=ok">Déconnexion</a>
    </div>

    <form method="POST">
        <div class="center-page2">
            <input type="text" name="montant" placeholder="Montant" maxlength="20"><br>
            <input type="text" name="annees" placeholder="Durée (années)"><br>
            <input type="text" name="taux" placeholder="Taux"><br>
            <input class="submit" type="submit" name="submit" value="Calculer">
        </div>
    </form>

    <?php

        $montant = isset($_POST['montant']) && !empty($_POST['montant']) ? $_POST['montant'] :0;
        $annees  = isset($_POST['annees'])  && !empty($_POST['annees'])  ? $_POST['annees']  :0;
        $taux    = isset($_POST['taux'])    && !empty($_POST['taux'])    ? $_POST['taux']    :0;
        $submit  = isset($_POST['submit']);

        if ($submit) {

    // Récapitulatif
        $montant_total_du = ($montant * 2) /100 + $montant;

        echo "<h3>Récapitulatif</h3>";
        echo "
            <div class='center'>
                <div><p>Montant</p><p>$montant €</p></div>
                <div><p>Durée</p><p>$annees an(s)</p></div>
                <div><p>Taux</p><p>$taux %</p></div>
                <div><p>Montant total dû</p><p>$montant_total_du €</p></div>
            </div>";

    // Echeancier
        $echeancier = $montant_total_du / ($annees * 12);
        $echeancier = round($echeancier, 2);
        $mois = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];

        echo "<h3>Echéancier</h3>";

        echo '<center class="container">';
            for ($i = 0; $i < $annees; $i++) {
                echo "<h4><div class='annees'>Pour l'année ".($i+2020)."</div></h4>";
    
                for ($j = 0; $j <= 11; $j++) {
                    echo "<div class='mois'>".$mois[$j].'<br><br>'.$echeancier." €</div>";
                }
            }
        echo "</center>";
    // REFUSER OU ACCEPTER
        echo '<footer class="inline-footer">
                <div class="red footer">
                    <a href="?sup=ok">REFUSER</a>
                </div>
                <div class="green footer">
                    <a href=""> ACCEPTER</a>
                </div>
            </footer>';

        }

    // DECONNEXION ET SUPPRESSION DES COOKIES
        $deconnecter = isset($_GET['sup']) ? $_GET['sup'] :0;
        if (!empty($deconnecter)) {
            if ($deconnecter == 'ok') {
                setcookie('pseudo', null, -1);
                header('Location: accord-de-pret.php');
            }
        }
 ?>
 
</body>
</html>